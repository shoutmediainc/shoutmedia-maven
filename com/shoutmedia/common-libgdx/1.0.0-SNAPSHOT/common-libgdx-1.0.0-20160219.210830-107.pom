<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<parent>
		<groupId>com.shoutmedia</groupId>
		<artifactId>common</artifactId>
		<version>1.0.0-SNAPSHOT</version>
	</parent>

	<modelVersion>4.0.0</modelVersion>
	<artifactId>common-libgdx</artifactId>
	<packaging>jar</packaging>

	<properties>
		<junit-version>4.11</junit-version>
		<spring.data.version>1.7.2.RELEASE</spring.data.version>
	</properties>

	<pluginRepositories>
		<pluginRepository>
			<id>synergian-repo</id>
			<url>https://raw.github.com/synergian/wagon-git/releases</url>
		</pluginRepository>
	</pluginRepositories>

	<dependencies>
		<dependency>
			<groupId>com.shoutmedia</groupId>
			<artifactId>common-core</artifactId>
			<version>1.0.0-SNAPSHOT</version>
		</dependency>

		<!-- LibGDX -->
		<dependency>
			<groupId>com.badlogicgames.gdx</groupId>
			<artifactId>gdx</artifactId>
			<version>1.9.1</version>
		</dependency>

		<!-- SQLDroid -->
		<dependency>
			<groupId>org.sqldroid</groupId>
			<artifactId>sqldroid</artifactId>
			<version>1.0.3</version>
			<scope>provided</scope>
		</dependency>

	</dependencies>

	<build>
		<finalName>${project.artifactId}</finalName>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${maven-compiler-plugin.version}</version>
				<configuration>
					<source>1.7</source>
					<target>1.7</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<version>2.5</version>
				<configuration>
					<encoding>UTF-8</encoding>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-eclipse-plugin</artifactId>
				<version>2.8</version>
				<configuration>
					<downloadSources>true</downloadSources>
					<downloadJavadocs>false</downloadJavadocs>
					<wtpversion>2.0</wtpversion>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-dependency-plugin</artifactId>
				<version>2.10</version>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>copy-dependencies</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
		<extensions>
			<extension>
				<groupId>ar.com.synergian</groupId>
				<artifactId>wagon-git</artifactId>
				<version>0.2.5</version>
			</extension>
		</extensions>
	</build>

	<distributionManagement>
		<repository>
			<id>shoutmediainc-common</id>
			<name>ShoutMedia Common Repository</name>
			<url>git:releases://git@bitbucket.org:shoutmediainc/shoutmedia-maven.git</url>
		</repository>
		<snapshotRepository>
			<id>shoutmediainc-maven-snapshots</id>
			<name>ShoutMedia Maven Snapshot Repository</name>
			<url>git:snapshots://git@bitbucket.org:shoutmediainc/shoutmedia-maven.git</url>
		</snapshotRepository>
	</distributionManagement>

</project>
